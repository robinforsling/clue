# Description

This folder contains MATLAB implementations for the robust optimization parts of the evaluation examples of the following paper:

Robin Forsling, Anders Hansson, Fredrik Gustafsson, Zoran Sjanic, Johan Löfberg and Gustaf Hendeby (2021) *Conservative Linear Unbiased Estimation*

**System requirements**:

* MATLAB
* YALMIP optimization toolbox: https://yalmip.github.io
* MOSEK solver: https://www.mosek.com