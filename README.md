# Conservative Linear Unbiased Estimation

This git repo contains files related to the *conservative linear unbiased estimator* (CLUE).

Folders:
* tsp-examples: Examples from the theory evaluation of the paper: Conservative Linear Unbiased Estimation Under Partially Known Covariances, submitted to *IEEE Transactions on Signal Processing*, 2022.
